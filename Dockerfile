FROM wordpress:latest

COPY --chown=www-data:www-data wordpress/wp-content/plugins /var/www/html/wp-content/plugins
COPY --chown=www-data:www-data wordpress/wp-content/themes/themomstudio /var/www/html/wp-content/themes/themomstudio
