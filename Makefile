dev:
	docker-compose up

build:
	docker build -t themomstudio/website:latest .
	
syncdb:
	./scripts/mysqlsync.sh
