# The Mom Studio Website

## Development

### Requirements

* [docker](https://docs.docker.com)
* [docker-compose](https://docs.docker.com/compose/)

### Running Wordpress localy

```
$ make dev
```
This will run [docker-compose](https://docs.docker.com/compose/), starting a
mysql container and a wordpress container.

To exec into the wordpress container you can run the following command:
```
$ docker exec -it $(docker ps -aqf "name=wordpress") /bin/bash
```

## Deployment

### Requirements

* [docker](https://docs.docker.com)
* [openssl](https://www.openssl.org/)

### Steps:

1. Clone the git folder to the server, where the website should run.
```
$ git clone https://gitlab.com/themomstudio/website.git
$ cd website
```

2. Create a secrets folder and add secrets
```
$ mkdir ../secrets
$ echo "my-db-user" > ../secrets/db-user
$ echo "my-db-password" > ../secrets/db-password
$ ./scripts/create-secrets.sh
$ rm  ../secrets/db-user
$ rm ../secrets/db-password
```

3. Run the docker images.
```
$ ./scripts/run.sh
```

### Updating

You can simply update the deployment by running:
```
$ git pull
$ ./scripts/run.sh
```
