#!/bin/bash

echo "Creating secret db-password:"
openssl enc -aes-256-cbc -pbkdf2 -in db-password -out db-password.enc

echo "Creating secret db-user:"
openssl enc -aes-256-cbc -pbkdf2 -in db-user -out db-user.enc
