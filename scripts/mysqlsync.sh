#!/bin/sh

[[ -f dblocal.sql.gz ]] && rm dblocal.sql.gz
[[ -f dblocal.sql ]] && rm dblocal.sql

ssh_port=${PORT:-22}

read -p 'DB User: ' db_user
read -sp 'DB Password: ' db_password
echo ""

echo "Create the mysqldump"
ssh -p $ssh_port $USER@themom.studio \
    "docker exec \$(docker ps -aqf 'name=themomstudio-website-db') \
    mysqldump --no-tablespaces -u $db_user --password=$db_password wordpressdb \
    | gzip -9 > dblocal.sql.gz"

echo "Download the mysqldump"
scp -P $ssh_port -l 8192 paul@themom.studio:/home/paul/dblocal.sql.gz .
gzip -d dblocal.sql.gz

echo "Load the mysqldump"
docker exec -i website_db_1 mysql \
    --user=exampleuser \
    --password=examplepass \
    exampledb < dblocal.sql
