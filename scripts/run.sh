#!/bin/sh

echo "Decrypt secret: db-user"
db_user=$(openssl enc -d -aes-256-cbc -pbkdf2 -in ../secrets/db-user.enc)
echo "Decrypt secret: db-pasword"
db_password=$(openssl enc -d -aes-256-cbc -pbkdf2 -in ../secrets/db-password.enc)

docker network create -d bridge themomstudio-website

make build

# Mysql DB
docker stop themomstudio-website-db
docker rm themomstudio-website-db

docker run \
    --name themomstudio-website-db \
    -d \
    -v $(pwd)/../data:/var/lib/mysql:rw \
    -e MYSQL_DATABASE=wordpressdb \
    -e MYSQL_USER=${db_user} \
    -e MYSQL_PASSWORD=${db_password} \
    -e MYSQL_RANDOM_ROOT_PASSWORD='1' \
    --network themomstudio-website \
    mysql:latest

# Wordpress
docker stop themomstudio-website-wp
docker rm themomstudio-website-wp

docker run \
    --name themomstudio-website-wp \
    -d \
    -p 8080:80 \
    -e WORDPRESS_DB_HOST=themomstudio-website-db \
    -e WORDPRESS_DB_USER=${db_user} \
    -e WORDPRESS_DB_PASSWORD=${db_password} \
    -e WORDPRESS_DB_NAME=wordpressdb \
    --network themomstudio-website \
    themomstudio/website:latest
