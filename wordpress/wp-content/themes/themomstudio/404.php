<?php
/**
 * File not found or web page not found template file.
 */

get_header();

echo "<p>Leider wurde diese Seite nicht gefunden :( </p>";

get_footer();
