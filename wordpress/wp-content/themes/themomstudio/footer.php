<?php
/**
* The theme footer.
*/
?>

</main><!--#main-->

<footer id="site-footer" class="site-footer page-footer">
	<div id="legal">
		<a href="/legal">legal</a>
	</div>
</footer><!--.page-footer-->


<!--wordpress footer-->
<?php wp_footer(); ?>
<!--end wordpress footer-->


</body>
</html>
