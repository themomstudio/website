<?php
//disable admin-bar
function hide_admin_bar(){ return false; }
add_filter( 'show_admin_bar', 'hide_admin_bar' );

//clean PHP-String
function clean($string) {
	return preg_replace('/[^a-zA-ZäöüÄÖÜß0-9 ]/', '', $string); // Removes special chars.
};

function theme_add_editor_styles() {
	add_editor_style( get_theme_file_uri( 'editor-style.css' , false ) );
};
add_action( 'admin_head', 'theme_add_editor_styles' );

function theMoM_styles() { 
    wp_enqueue_style('mytheme_mobile_style', get_theme_file_uri('wp-base-style.css'));
	wp_enqueue_style('mytheme_mobile_style', get_theme_file_uri('css-reset.css'));
	wp_enqueue_style('mytheme_main_style', get_stylesheet_uri()); 
} 
add_action('wp_enqueue_scripts', 'theMoM_styles');

// include custom jQuery
function include_custom_jquery() {

	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js', array(), null, false);

}
add_action('wp_enqueue_scripts', 'include_custom_jquery');