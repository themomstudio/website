<?php
/**
* The theme header.
*/


?>
<!DOCTYPE html>
<html class="no-js" lang="<?php echo $lang ?>">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<title></title>
	

	<!--wordpress head-->
	<?php wp_head(); ?>
	<!--end wordpress head-->
</head>

<body <?php body_class(); ?>>
	<div class="page-container">
		<header class="page-header">
			
		</header><!--.page-header-->

	<main id="main" class="site-main" role="main"><!--#main-->
