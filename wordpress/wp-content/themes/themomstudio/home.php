<?php
/*
Template Name: home
*/


get_header();

?>

<div id="PageContent">

<?php 

$FrontPageList = array();

##Automatically fetch all php files in front-pages directory
$Directory = new RecursiveDirectoryIterator(dirname(__FILE__).'/inc_front-pages');
$Iterator = new RecursiveIteratorIterator($Directory);
$FrontPages = new RegexIterator($Iterator, '/^.+\.php$/i', RecursiveRegexIterator::GET_MATCH);
foreach($FrontPages as $name => $object){
    $FrontPageList[] = "$name";
};


$randPage = rand(0,count($FrontPageList)-1);

while ($randPage == $_COOKIE["LastPage"]) {
    $randPage = rand(0,count($FrontPageList)-1);
};

require_once $FrontPageList[$randPage];



?>

</div>
<script>
    document.cookie = "LastPage=<?php echo $randPage; ?>"; 
</script>
<?php

get_footer();