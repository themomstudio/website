<?php
	$pod = pods('page', get_the_id());
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/inc_front-pages/timo-01/style.css">

<div id="custom-page">
	<div class="address">
		<?php echo $pod->field( 'address' ) ?>
	</div>
	<h1>
		<?php echo get_the_title(); ?>
	</h1>

	<div class="p_right_block">
		<?php the_content();?>
	</div>
	
</div>
<?php