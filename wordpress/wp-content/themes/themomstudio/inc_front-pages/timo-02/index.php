<?php
	$pod = pods('page', get_the_id());
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/inc_front-pages/timo-02/style.css">

<div id="custom-page" class="custom-page">
	<canvas id="canvas2d"></canvas>
	<div class="fade"></div>
	<div class="address">
		<?php echo $pod->field( 'address' ) ?>
	</div>
	<div class="star-wars">
		<div class="crawl">
			<div class="title">
				<h1>
					<?php echo get_the_title(); ?>
				</h1>
			</div>

			<div class="content">
				<?php the_content();?>
			</div>
			
			<div class="title">
				<h1>
					<?php echo get_the_title(); ?>
				</h1>
			</div>

			<div class="content">
				<?php the_content();?>
			</div>
			
			<div class="title">
				<h1>
					<?php echo get_the_title(); ?>
				</h1>
			</div>

			<div class="content">
				<?php the_content();?>
			</div>
			
			<div class="title">
				<h1>
					<?php echo get_the_title(); ?>
				</h1>
			</div>

			<div class="content">
				<?php the_content();?>
			</div>
			
			<div class="title">
				<h1>
					<?php echo get_the_title(); ?>
				</h1>
			</div>

			<div class="content">
				<?php the_content();?>
			</div>
			
			<div class="title">
				<h1>
					<?php echo get_the_title(); ?>
				</h1>
			</div>

			<div class="content">
				<?php the_content();?>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo get_template_directory_uri(); ?>/inc_front-pages/timo-02/main.js"></script> 
<?php