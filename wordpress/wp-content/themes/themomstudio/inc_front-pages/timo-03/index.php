<?php
##Automatically fetch all php files in front-pages directory
$Directory = new RecursiveDirectoryIterator(dirname(__FILE__).'/img');
$Iterator = new RecursiveIteratorIterator($Directory);
$MomBG = new RegexIterator($Iterator, '/^.+\.jpg$/i', RecursiveRegexIterator::GET_MATCH);
foreach($MomBG as $name => $object){
    $MomBGs[] = basename("$name");
};
natsort($MomBGs);
$MomBGs = array_values($MomBGs);

$pod = pods('page', get_the_id());
?>
<link rel="stylesheet" type="text/css"
    href="<?php echo get_template_directory_uri(); ?>/inc_front-pages/timo-03/style.css">

<div id="custom-page">
    <div class="address">
        <?php echo $pod->field( 'address' ) ?>
    </div>
    <h1 class="hidden">
        <?php echo get_the_title(); ?>
    </h1>

    <div class="text_block">
        <?php the_content();?>
    </div>

</div>

<script>
    $('body').mousemove(function(mouse){
        var mousePos = event.pageX;
        var pageChunk = Math.floor(mousePos * <?php echo count($MomBGs); ?> / $('body').width());
        switch (pageChunk) {
            <?php
            for ($i=0; $i < count($MomBGs); $i++) { 
                echo "
                case $i:
                    $('body').css('background-image', 'url(\"".get_template_directory_uri()."/inc_front-pages/timo-03/img/".$MomBGs[$i]."\")');
                    break;
                ";
            };
            ?>

            default:
                $('body').css('background-image', 'url("<?php echo get_template_directory_uri() ?>/inc_front-pages/timo-03/img/<?php echo $MomBGs[0] ?>")');
                break;
        }
    });
    
</script>

<?php