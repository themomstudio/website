<?php

/*
Template Name: full-page plain
*/

get_header();

?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/wp_templates/full-page.css">

<div id="PageContent" class="full-page">

<h1><?php echo get_the_title(); ?></h1>

	<?php
	while (have_posts()) {
		the_post();
		the_content();
	};
		?>

</div>

<?php

get_footer();
